require('dotenv').config();
const { ethers } = require('ethers');
const { Token } = require('@uniswap/sdk-core');
const { JsonRpcProvider } = require('@ethersproject/providers');
const { Pool, TickMath, nearestUsableTick } = require('@uniswap/v3-sdk');
const NonfungiblePositionManagerABItest = require('./NonfungiblePositionManagerTest.json');

const PRIVATE_KEY = process.env.PRIVATE_KEY;
const provider = new JsonRpcProvider(process.env.INFURA_URL);
const wallet = new ethers.Wallet(PRIVATE_KEY, provider);

const SEPOLIA_CHAIN_ID = parseInt(process.env.CHAIN_ID) || 11155111;

const tokenA = new Token(SEPOLIA_CHAIN_ID, '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2', 18, 'WETH', 'Wrapped Ether');
const tokenB = new Token(SEPOLIA_CHAIN_ID, '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48', 18, 'USDC', 'USD Coin');

const fee = 3000;

async function createPool() {
    const sqrtPriceX96 = TickMath.getSqrtRatioAtTick(nearestUsableTick(0, 1));
    const pool = new Pool(
        tokenA,
        tokenB,
        fee,
        sqrtPriceX96,
        0,
        0
    );

    const positionManagerAbi = new ethers.Contract(
        '0xC36442b4a4522E871399CD717aBDD847Ab11FE88',
        NonfungiblePositionManagerABItest,
        wallet
    );

    try {
        const balance = await wallet.getBalance();
        console.log('Số dư ví:', ethers.utils.formatEther(balance), 'ETH');

        const estimatedGasLimit = await positionManagerAbi.estimateGas.createAndInitializePoolIfNecessary(
            tokenA.address,
            tokenB.address,
            fee,
            sqrtPriceX96
        );
        console.log('Giới hạn gas ước tính:', estimatedGasLimit.toString());

        const gasPrice = await provider.getGasPrice();
        if (balance.lt(estimatedGasLimit.mul(gasPrice))) {
            throw new Error('Số dư không đủ để thanh toán phí gas');
        }

        const tx = await positionManagerAbi.createAndInitializePoolIfNecessary(
            tokenA.address,
            tokenB.address,
            fee,
            sqrtPriceX96,
            { gasLimit: 50000 }
        );
        
        await tx.wait();
        console.log('Đã tạo pool:', tx.hash);
    } catch (error) {
        console.error("Lỗi khi tạo pool:", error);
    }
}

createPool().catch(console.error);
