const express = require('express');
const cors = require('cors');
const ethers = require('ethers');
const dotenv = require('dotenv');
const { JsonRpcProvider } = require('@ethersproject/providers');
const NonfungiblePositionManagerABI = require('./NonfungiblePositionManager.json');
const ERC20_ABI = require('./ERC20.json');
const WETH_ABI = require('./WETH.json');
const IUniswapV3PoolABI = require('@uniswap/v3-core/artifacts/contracts/interfaces/IUniswapV3Pool.sol/IUniswapV3Pool.json');
const { Percent, Token: TokenCore } = require('@uniswap/sdk-core');
const { NonfungiblePositionManager, Pool, Position, computePoolAddress, nearestUsableTick } = require('@uniswap/v3-sdk');
const FactoryABI = require('./FactoryABI.json');
const { BigNumber } = require('ethers');
const SwapRouterABI = require('./SwapRouterABI.json');

dotenv.config();
const app = express();
app.use(cors());
app.use(express.static('public'));
app.use(express.json());

const PORT = process.env.PORT || 3000;

// const poolFee = 3000; // 0.3%
// // const factoryAddress = '0x1F98431c8aD98523631AE4a59f267346ea31F984'; // Mainnet factory address
// // const token0Address = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2'; // WETH9 main address
// // const token1Address = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'; // USDC main address

// const factoryAddress = '0x0227628f3F023bb0B980b67D528571c95c6DaC1c'; // Test factory address
// const token0Address = '0xfff9976782d46cc05630d1f6ebab18b2324d6b14'; // WETH9 test address
// const token1Address = '0x94a9D9AC8a22534E3FaCa9F4e7F2E2cf85d5E4C8'; // USDC test address
// const chainId = 11155111; // Ethereum mainnet chain ID
// const token0 = new TokenCore(chainId, token0Address, 18, 'WETH', 'Wrapped Ether');
// const token1 = new TokenCore(chainId, token1Address, 6, 'USDC', 'USD Coin');
// const provider = new JsonRpcProvider(process.env.SEPOLIA_INFURA_URL);

const networkConfig = {
  ethereum: {
    factoryAddress: '0x1F98431c8aD98523631AE4a59f267346ea31F984', // Mainnet factory address
    token0Address: '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2', // WETH9 main address
    token1Address: '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48', // USDC main address
    positionManagerAddress: '0xC36442b4a4522E871399CD717aBDD847Ab11FE88', // Mainnet NonfungiblePositionManager contract address
    swapRouterAddress: '0xE592427A0AEce92De3Edee1F18E0157C05861564', // Mainnet SwapRouter contract address
    provider: new JsonRpcProvider(process.env.MAINNET_INFURA_URL),
    chainId: 1,
    poolFee: 3000 // 0.3%
  },
  polygon: {
    factoryAddress: '0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32', // Replace with actual Polygon factory address
    token0Address: '0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619', // Replace with actual Polygon WETH address
    token1Address: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174', // Replace with actual Polygon USDC address
    positionManagerAddress: '0xC36442b4a4522E871399CD717aBDD847Ab11FE88', // Replace with actual Polygon NonfungiblePositionManager address
    swapRouterAddress: '0xf5b509bB0909a69B1c207E495f687a596C168E12', // Replace with actual Polygon SwapRouter address
    provider: new JsonRpcProvider(process.env.POLYGON_INFURA_URL),
    chainId: 137,
    poolFee: 3000 // 0.3%
  },
  sepolia: {
    factoryAddress: '0x0227628f3F023bb0B980b67D528571c95c6DaC1c', // Test factory address
    token0Address: '0xfff9976782d46cc05630d1f6ebab18b2324d6b14', // WETH9 test address
    token1Address: '0x94a9D9AC8a22534E3FaCa9F4e7F2E2cf85d5E4C8', // USDC test address
    positionManagerAddress: '0x1238536071E1c677A632429e3655c799b22cDA52', // Test NonfungiblePositionManager contract address
    swapRouterAddress: '0xE592427A0AEce92De3Edee1F18E0157C05861564', // Test SwapRouter contract address
    provider: new JsonRpcProvider(process.env.SEPOLIA_INFURA_URL),
    chainId: 11155111,
    poolFee: 3000 // 0.3%
  }
};

let currentNetwork = 'ethereum';
let wallet = new ethers.Wallet(process.env.PRIVATE_KEY || '', networkConfig[currentNetwork].provider);
let positionManager = new ethers.Contract(networkConfig[currentNetwork].positionManagerAddress, NonfungiblePositionManagerABI, wallet);
const swapRouterAddress = networkConfig[currentNetwork].swapRouterAddress; // Update to use the correct address
let swapContract = new ethers.Contract(swapRouterAddress, SwapRouterABI, wallet);
const updateNetworkConfig = (network) => {
  if (networkConfig[network]) {
    currentNetwork = network;
    wallet = new ethers.Wallet(wallet.privateKey, networkConfig[currentNetwork].provider);
  } else {
    throw new Error('Invalid network');
  }
};

app.post('/setNetwork', (req, res) => {
  const { network } = req.body;
  try {
    updateNetworkConfig(network);
    console.log(`Network set to ${network}`); // Log network change
    res.send({ message: `Network set to ${network}` });
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
});

app.post('/setPrivateKey', (req, res) => {
  const { privateKey } = req.body;
  try {
    wallet = new ethers.Wallet(privateKey, networkConfig[currentNetwork].provider);
    console.log('Private key set successfully'); // Log private key change
    res.send({ message: 'Private key set successfully' });
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
});
app.get('/getCurrentNetwork', (req, res) => {
  res.send({ network: currentNetwork });
});
app.get('/getPrivateKey', (req, res) => {
    res.send({ privateKey: wallet.privateKey });
});

app.post('/checkBalance', async (req, res) => {
  const { privateKey } = req.body;
  if (!privateKey) {
    return res.status(400).send({ message: 'Private key is required' });
  }

  try {
    const tempWallet = new ethers.Wallet(privateKey, networkConfig[currentNetwork].provider);
    const balance = await tempWallet.getBalance();
    res.send({ balance: ethers.utils.formatEther(balance) });
  } catch (error) {
    console.error('Error checking balance:', error);
    res.status(500).send({ message: 'Error checking balance', error: error.message });
  }
});

// const positionManager = new ethers.Contract(
//   '0xC36442b4a4522E871399CD717aBDD847Ab11FE88', // Mainnet NonfungiblePositionManager contract address
//   NonfungiblePositionManagerABI,
//   wallet
// );

// const positionManager = new ethers.Contract(
//   '0x1238536071E1c677A632429e3655c799b22cDA52', // Test NonfungiblePositionManager contract address
//   NonfungiblePositionManagerABI,
//   wallet
// );

// const swapContract = new ethers.Contract(
//   '0xE592427A0AEce92De3Edee1F18E0157C05861564', // Mainnet SwapRouter contract address
//   SwapRouterABI, 
//   wallet 
// );

const checkPoolExists = async () => {
  const poolAddress = computePoolAddress({
    factoryAddress: networkConfig[currentNetwork].factoryAddress,
    tokenA: new TokenCore(networkConfig[currentNetwork].chainId, networkConfig[currentNetwork].token0Address, 18, 'WETH', 'Wrapped Ether'),
    tokenB: new TokenCore(networkConfig[currentNetwork].chainId, networkConfig[currentNetwork].token1Address, 6, 'USDC', 'USD Coin'),
    fee: networkConfig[currentNetwork].poolFee,
  });

  console.log('Computed pool address:', poolAddress);

  try {
    const poolContract = new ethers.Contract(poolAddress, IUniswapV3PoolABI.abi, networkConfig[currentNetwork].provider);
    const code = await networkConfig[currentNetwork].provider.getCode(poolContract.address);

    const poolExists = code !== '0x';

    return {
      poolExists,
      poolAddress,
      contractCode: code,
    };
  } catch (error) {
    console.error('Error checking pool existence:', error);
    return {
      poolExists: false,
      poolAddress,
      error: error.message,
    };
  }
};

let poolStatus = null;

const updatePoolStatus = async () => {
  poolStatus = await checkPoolExists();
  console.log('Pool status updated:', poolStatus);
};
setInterval(updatePoolStatus, 300000);

updatePoolStatus();

app.get('/checkPoolExists', async (req, res) => {
  try {
    const result = await checkPoolExists();
    res.send(result);
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: 'Error checking pool existence', error: err.message });
  }
});

const approveToken = async (token, amount) => {
  const tokenContract = new ethers.Contract(token, ERC20_ABI, wallet);
  const allowance = await tokenContract.allowance(wallet.address, networkConfig[currentNetwork].positionManagerAddress);
  if (allowance.lt(amount)) {
    console.log('Approving token transfer: ', token, 'amount: ', amount.toString());
    const tx = await tokenContract.approve(networkConfig[currentNetwork].positionManagerAddress, amount);
    await tx.wait();
  }
};

const checkBalance = async () => {
  const balance = await wallet.getBalance();
  console.log(`Wallet balance: ${ethers.utils.formatEther(balance)} ETH`);
  return balance;
};

const getTokenBalance = async (token, address) => {
  const contract = new ethers.Contract(token, ERC20_ABI, wallet);
  const balance = await contract.balanceOf(address);
  console.log(`Token balance for ${token}: ${ethers.utils.formatUnits(balance, await contract.decimals())}`);
  return balance;
};

// Function to wrap ETH to WETH
const wrapETH = async (amount) => {
  const wethContract = new ethers.Contract(networkConfig[currentNetwork].token0Address, WETH_ABI, wallet);
  const tx = await wethContract.deposit({ value: ethers.utils.parseEther(amount.toString()) });
  await tx.wait();
  console.log(`Wrapped ${amount} ETH to WETH`);
};

const calculateETHAmount = (usdcAmount, minprice, maxprice) => {
  const averagePrice = (parseFloat(minprice) + parseFloat(maxprice)) / 2;
  const ethAmount = parseFloat(usdcAmount) / averagePrice;
  return ethAmount;
};

const main = async (usdcAmount, minprice, maxprice) => {
  const ethAmount = calculateETHAmount(usdcAmount, minprice, maxprice);

  const formattedEthAmount = parseFloat(ethAmount).toFixed(18);
  const formattedUsdcAmount = parseFloat(usdcAmount).toFixed(6);

  const amount0Desired = ethers.utils.parseUnits(formattedEthAmount, 18); // WETH
  const amount1Desired = ethers.utils.parseUnits(formattedUsdcAmount, 6); // USDC

  const token0Balance = await getTokenBalance(networkConfig[currentNetwork].token0Address, wallet.address); // WETH
  const token1Balance = await getTokenBalance(networkConfig[currentNetwork].token1Address, wallet.address); // USDC

  // Kiểm tra và wrap ETH nếu cần thiết
  if (token0Balance.lt(amount0Desired)) {
    const ethBalance = await checkBalance();
    if (ethBalance.lt(amount0Desired)) {
      console.log('ETH balance is not enough to wrap to WETH');
      throw new Error('ETH balance is not enough to wrap to WETH');
    }
    await wrapETH(ethers.utils.formatEther(amount0Desired.sub(token0Balance))); // Wrap only the required amount
  }

  const updatedToken0Balance = await getTokenBalance(networkConfig[currentNetwork].token0Address, wallet.address); // WETH sau khi wrap
  if (updatedToken0Balance.lt(amount0Desired)) {
    throw new Error('Token0 balance (WETH) is not enough');
  }

  if (token1Balance.lt(amount1Desired)) {
    throw new Error('Token1 balance (USDC) is not enough');
  }

  // Phê duyệt số lượng token WETH và USDC
  await approveToken(networkConfig[currentNetwork].token0Address, amount0Desired); // Phê duyệt WETH
  await approveToken(networkConfig[currentNetwork].token1Address, amount1Desired); // Phê duyệt USDC

  const currentPoolAddress = computePoolAddress({
    factoryAddress: networkConfig[currentNetwork].factoryAddress,
    tokenA: new TokenCore(networkConfig[currentNetwork].chainId, networkConfig[currentNetwork].token0Address, 18, 'WETH', 'Wrapped Ether'),
    tokenB: new TokenCore(networkConfig[currentNetwork].chainId, networkConfig[currentNetwork].token1Address, 6, 'USDC', 'USD Coin'),
    fee: networkConfig[currentNetwork].poolFee,
  });

  const poolContract = new ethers.Contract(currentPoolAddress, IUniswapV3PoolABI.abi, networkConfig[currentNetwork].provider);

  const [liquidity, slot0] = await Promise.all([
    poolContract.liquidity(),
    poolContract.slot0(),
  ]);
  const configuredPool = new Pool(
    new TokenCore(networkConfig[currentNetwork].chainId, networkConfig[currentNetwork].token0Address, 18, 'WETH', 'Wrapped Ether'),
    new TokenCore(networkConfig[currentNetwork].chainId, networkConfig[currentNetwork].token1Address, 6, 'USDC', 'USD Coin'),
    networkConfig[currentNetwork].poolFee,
    slot0.sqrtPriceX96.toString(),
    liquidity.toString(),
    slot0.tick
  );

  // Tạo đối tượng Position với số lượng token đúng
  const position = Position.fromAmounts({
    pool: configuredPool,
    tickLower: nearestUsableTick(configuredPool.tickCurrent, configuredPool.tickSpacing) - configuredPool.tickSpacing * 2,
    tickUpper: nearestUsableTick(configuredPool.tickCurrent, configuredPool.tickSpacing) + configuredPool.tickSpacing * 2,
    amount0: amount1Desired.toString(),
    amount1: amount0Desired.toString(),
    useFullPrecision: true,
  });
  console.log('Position is: ', position);

  const mintOptions = {
    recipient: wallet.address,
    deadline: Math.floor(Date.now() / 1000) + 60 * 20,
    slippageTolerance: new Percent(100, 10_000),
  };
  console.log('Mint options: ', mintOptions);

  const { calldata, value } = NonfungiblePositionManager.addCallParameters(position, mintOptions);

  const baseGasPrice = await networkConfig[currentNetwork].provider.getGasPrice();
  const gasPrice = baseGasPrice.mul(250).div(100);
  const transaction = {
    data: calldata,
    to: networkConfig[currentNetwork].positionManagerAddress,
    value: value,
    from: wallet.address,
    gasPrice: gasPrice,
  };
  console.log('Transaction: ', transaction);

  try {
    const txRes = await wallet.sendTransaction(transaction);
    console.log('Transaction hash: ', txRes.hash);
  } catch (error) {
    console.error('Error sending transaction:', error);
  }
};

app.post('/mintPosition', async (req, res) => {
  try {
    const { amount2, minprice, maxprice } = req.body;
    await main(amount2, minprice, maxprice);
    res.send({ message: 'Position minted successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: 'Error minting position', error: err.message });
  }
});


app.get('/checkPoolExists', async (req, res) => {
  try {
    const result = await checkPoolExists();
    res.send(result);
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: 'Error checking pool existence', error: err.message });
  }
});



async function getPositionData(tokenId) {
  if (!tokenId) {
    throw new Error('Token ID is required');
  }

  try {
    const position = await positionManager.positions(tokenId);
    return position;
  } catch (error) {
    console.error('Error fetching position data:', error);
    console.log('Liquidity123123:', position.liquidity.toString());

    throw new Error('Failed to fetch position data');
  }
}



app.post('/getPositionData', async (req, res) => {
  const tokenId = req.body.tokenId;

  if (tokenId === undefined) {
    return res.status(400).send({ message: 'Token ID is required' });
  }

  try {
    const positionData = await getPositionData(tokenId);
    res.send({ message: 'Position data fetched successfully', positionData });
    console.log('Liquidity:', position.liquidity.toString());

  } catch (error) {
    console.error('Error fetching position data:', error);
    res.status(500).send({ message: 'Failed to fetch position data', error: error.message, stack: error.stack });
  }
});





const addLiquidity = async (tokenId, liquidity, amount0, amount1) => {
  if (!tokenId || !liquidity || !amount0 || !amount1) {
    throw new Error('All parameters are required');
  }

  if (typeof tokenId !== 'number' || typeof liquidity !== 'number' || typeof amount0 !== 'number' || typeof amount1 !== 'number') {
    throw new Error('All parameters must be numbers');
  }

  const amount0Desired = ethers.utils.parseUnits(amount0.toFixed(18), 18); // Assuming WETH has 18 decimals
  const amount1Desired = ethers.utils.parseUnits(amount1.toFixed(6), 6); // Assuming USDC has 6 decimals
  console.log(amount0Desired.toString());
  console.log(amount1Desired.toString());
  const token0Balance = await getTokenBalance(networkConfig[currentNetwork].token0Address, wallet.address); // WETH
  const token1Balance = await getTokenBalance(networkConfig[currentNetwork].token1Address, wallet.address); // USDC
console.log(token0Balance.toString());
console.log(token1Balance.toString());
  // Check if we need to wrap ETH to WETH
  if (token0Balance.lt(amount0Desired)) {
    const ethBalance = await checkBalance();
    if (ethBalance.lt(amount0Desired)) {
      console.log('ETH balance is not enough to wrap to WETH');
      throw new Error('ETH balance is not enough to wrap to WETH');
    }
    await wrapETH(ethers.utils.formatEther(amount0Desired.sub(token0Balance))); // Wrap only the required amount
  }

  const updatedToken0Balance = await getTokenBalance(networkConfig[currentNetwork].token0Address, wallet.address); // WETH after wrapping
  if (updatedToken0Balance.lt(amount0Desired)) {
    throw new Error('Token0 balance (WETH) is not enough');
  }

  if (token1Balance.lt(amount1Desired)) {
    throw new Error('Token1 balance (USDC) is not enough');
  }

  // Approve WETH and USDC if necessary
  await approveToken(networkConfig[currentNetwork].token0Address, amount0Desired); // Approve WETH
  await approveToken(networkConfig[currentNetwork].token1Address, amount1Desired); // Approve USDC

  const params = {
    tokenId,
    liquidity,
    amount0Desired,
    amount1Desired,
    amount0Min: 0,
    amount1Min: 0,
    deadline: Math.floor(Date.now() / 1000) + 60 * 10, // 10-minute deadline
  };

  const { calldata, value } = positionManager.interface.encodeFunctionData("increaseLiquidity", [params]);

  const baseGasPrice = await networkConfig[currentNetwork].provider.getGasPrice();
  const gasPrice = baseGasPrice.mul(300).div(100);

  const transaction = {
    data: calldata,
    to: networkConfig[currentNetwork].positionManagerAddress,
    value: value,
    from: wallet.address,
    gasPrice: gasPrice,
  };

  try {
    const tx = await wallet.sendTransaction(transaction);
    console.log('Transaction hash:', tx.hash); // Log the transaction hash
    await tx.wait(); // Wait for the transaction to be mined

    console.log('Transaction confirmed in block:', tx.blockNumber); // Log the block number
    return tx;
  } catch (error) {
    console.error('Error adding liquidity:', error);
    throw error;
  }
};

app.post('/addLiquidity', async (req, res) => {
  const { tokenId, liquidity, amount0, amount1 } = req.body;
  try {
    const tx = await addLiquidity(tokenId, liquidity, amount0, amount1);
    res.send({ message: 'Liquidity added successfully', tx });
  } catch (error) {
    console.error('Error adding liquidity:', error);
    res.status(500).send({ message: 'Failed to add liquidity', error: error.message, stack: error.stack });
  }
});





const removeLiquidity = async (tokenId, liquidity) => {
  try {
    const balance = await checkBalance();
    const requiredBalance = ethers.utils.parseEther('0.01');

    if (balance.lt(requiredBalance)) {
      throw new Error('Insufficient funds in the wallet');
    }

    const params = {
      tokenId,
      liquidity,
      amount0Min: 0,
      amount1Min: 0,
      deadline: Math.floor(Date.now() / 1000) + 60 * 10, // 10-minute deadline
    };

    const baseGasPrice = await networkConfig[currentNetwork].provider.getGasPrice();
    const gasPrice = baseGasPrice.mul(250).div(100);

    const transaction = {
      data: positionManager.interface.encodeFunctionData("decreaseLiquidity", [params]),
      to: networkConfig[currentNetwork].positionManagerAddress,
      value: ethers.BigNumber.from("0"), // Typically no ETH is sent with decreaseLiquidity
      from: wallet.address,
      gasPrice: gasPrice,
    };

    const tx = await wallet.sendTransaction(transaction);
    console.log('Transaction hash:', tx.hash); // Log the transaction hash

    await tx.wait(); // Wait for the transaction to be mined
    console.log('Transaction confirmed in block:', tx.blockNumber); // Log the block number

    return tx;
  } catch (error) {
    console.error('Error removing liquidity:', error);
    throw error;
  }
};

app.post('/removeLiquidity', async (req, res) => {
  const { tokenId, liquidity } = req.body;
  
  if (!tokenId || !liquidity) {
    return res.status(400).send({ message: 'tokenId and liquidity are required' });
  }

  try {
    const tx = await removeLiquidity(tokenId, liquidity);
    res.send({ message: 'Liquidity removed successfully', tx });
  } catch (error) {
    console.error('Error removing liquidity:', error);
    res.status(500).send({ message: 'Failed to remove liquidity', error: error.message, stack: error.stack, params: { tokenId, liquidity } });
  }
});



app.post('/getLiquidityFees', async (req, res) => {
  const { tokenId } = req.body;
  try {
    const fees = await getLiquidityFees(tokenId);
    // Convert hexadecimal values to decimal
    fees.feeGrowthInside0LastX128 = BigNumber.from(fees.feeGrowthInside0LastX128).toString();
    fees.feeGrowthInside1LastX128 = BigNumber.from(fees.feeGrowthInside1LastX128).toString();
    fees.tokensOwed0 = BigNumber.from(fees.tokensOwed0).toString();
    fees.tokensOwed1 = BigNumber.from(fees.tokensOwed1).toString();
    res.send({ message: 'Liquidity fees retrieved successfully', fees });
  } catch (error) {
    console.error('Error retrieving liquidity fees:', error);
    res.status(500).send({ message: 'Failed to retrieve liquidity fees', error: error.message, stack: error.stack });
  }
});

async function getLiquidityFees(tokenId) {
  try {
    const position = await positionManager.positions(tokenId);
    const feeGrowthInside0LastX128 = position.feeGrowthInside0LastX128;
    const feeGrowthInside1LastX128 = position.feeGrowthInside1LastX128;
    const tokensOwed0 = position.tokensOwed0;
    const tokensOwed1 = position.tokensOwed1;

    return {
      feeGrowthInside0LastX128,
      feeGrowthInside1LastX128,
      tokensOwed0,
      tokensOwed1
    };
  } catch (error) {
    throw new Error('Error getting liquidity fees: ' + error.message);
  }
}


async function swap(tokenIn, tokenOut, amountIn, amountOut, recipient) {
  try {
    // Gọi hàm swapExactTokensForTokens trên contract
    const tx = await swapContract.swapExactTokensForTokens(
      amountIn, 
      amountOut, 
      [tokenIn, tokenOut], 
      recipient, 
      { gasLimit: 300000 }
    );

    await tx.wait();
    console.log('Swap thành công!');
  } catch (error) {
    console.error('Swap thất bại:', error);
    throw error; 
  }
}

// Endpoint POST để thực hiện swap
app.post('/swap', async (req, res) => {
  try {
    const { tokenIn, tokenOut, amountIn, amountOut, recipient } = req.body;
    await swap(tokenIn, tokenOut, amountIn, amountOut, recipient);
    res.send({ message: 'Swap executed successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: 'Error executing swap', error: err.message });
  }
});








app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
